package com.tangcheng.adapter.db.user.dao.mapper;


import com.alibaba.fastjson.JSON;
import com.tangcheng.adapter.db.user.dao.biz.BizSysAccountMapper;
import com.tangcheng.adapter.db.user.dao.entity.SysAccount;
import com.tangcheng.adapter.db.user.dao.entity.SysAccountExample;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @Auther: cheng.tang
 * @Date: 2024/8/9
 * @Description: mybatis-subject
 */
@SpringBootTest
@Slf4j
public class SysAccountMapperTest {

    @Resource
    private BizSysAccountMapper bizSysAccountMapper;

    @Test
    public void insert() {
        SysAccountExample example = new SysAccountExample();
        example.createCriteria().andEmailEqualTo("cheng.tang@ai-as.net")
                .andNickNameEqualTo("AIGC");
        List<SysAccount> sysAccounts = bizSysAccountMapper.selectByExample(example);
        log.info(" {} ", JSON.toJSONString(sysAccounts));
        if (CollectionUtils.isEmpty(sysAccounts)) {
            SysAccount record = new SysAccount();
            record.setNickName("AIGC");
            record.setLoginName("测试");
            record.setPwd("123456");
            record.setEmail("cheng.tang@ai-as.net");
            record.setAvatar("");
            record.setProfile("无");
            record.setCreatorBy(1L);
            record.setModifiedBy(1L);
            bizSysAccountMapper.insert(record);
            insert();
        }
    }


    @Test
    public void selectByPrimaryKey() {
        /**
         * {"avatar":"","creatorBy":1,"email":"cheng.tang@ai-as.net","id":14,"loginName":"测试","modifiedBy":1,"nickName":"AIGC","profile":"无","pwd":"123456"}
         */
        SysAccount record = new SysAccount();
        record.setProfile("个人介绍");
        record.setEmail("cheng.tang@ai-as.cn");

        SysAccountExample example = new SysAccountExample();
        example.createCriteria().andIdIn(Arrays.asList(1L, 14L))
                .andEmailIn(Arrays.asList("cheng.tang@ai-as.net", "cheng.tang@ai-as.cn"));
        bizSysAccountMapper.updateByExampleSelective(record, example);
    }


}