package com.tangcheng.adapter.db.mybatis.plugin.sensitive.service;

import com.tangcheng.adapter.db.mybatis.plugin.sensitive.util.AesBase64Util;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @Auther: cheng.tang
 * @Date: 2024/8/15
 * @Description: mybatis-subject
 */
class AESBase64EncryptionAlgorithmTest {

    @Test
    void encrypt() {
        String result = AesBase64Util.encrypt("test123789", "023439728992");
        assertThat(result).isEqualTo("8naGTllud+8tco4yns+W9iF9ds8uqE=");
    }


}