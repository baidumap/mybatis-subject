package com.tangcheng.adapter.db.user.dao.biz;

import com.alibaba.fastjson.JSON;
import com.tangcheng.SpringBootMybatisApplication;
import com.tangcheng.adapter.db.user.dao.entity.SensitiveField;
import com.tangcheng.adapter.db.user.dao.entity.SensitiveFieldExample;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Auther: cheng.tang
 * @Date: 2024/8/15
 * @Description: mybatis-subject
 */
@SpringBootTest(classes = SpringBootMybatisApplication.class)
@Slf4j
class BizSensitiveFieldMapperTest {

    @Resource
    private BizSensitiveFieldMapper bizSensitiveFieldMapper;

    @Test
    public void testSelectByExample() {
        List<SensitiveField> sensitiveFieldList = bizSensitiveFieldMapper.selectByExample(new SensitiveFieldExample());
        log.info(" {} ", JSON.toJSONString(sensitiveFieldList));
    }

}