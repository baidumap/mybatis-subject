package com.tangcheng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Auther: cheng.tang
 * @Date: 2024/8/9
 * @Description: mybatis-subject
 */
//@ImportResource({"classpath:mybatis/applicationContext-*.xml"})
@MapperScan("com.tangcheng.adapter.db")
@SpringBootApplication
public class SpringBootMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMybatisApplication.class, args);
    }

}
