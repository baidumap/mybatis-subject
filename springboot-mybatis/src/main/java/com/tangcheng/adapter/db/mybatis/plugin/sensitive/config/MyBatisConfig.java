package com.tangcheng.adapter.db.mybatis.plugin.sensitive.config;

import com.tangcheng.adapter.db.mybatis.plugin.sensitive.SensitiveFieldInterceptor;
import com.tangcheng.adapter.db.mybatis.plugin.sensitive.service.AESBase64EncryptionAlgorithm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Auther: cheng.tang
 * @Date: 2024/8/15
 * @Description: mybatis-subject
 */
@Configuration
public class MyBatisConfig {

    @Bean
    public SensitiveFieldInterceptor sensitiveFieldInterceptor(AESBase64EncryptionAlgorithm aesBase64EncryptionAlgorithm) {
        SensitiveFieldInterceptor interceptor = new SensitiveFieldInterceptor();
        interceptor.setEncryptionAlgorithm(aesBase64EncryptionAlgorithm);
        return interceptor;
    }

}
