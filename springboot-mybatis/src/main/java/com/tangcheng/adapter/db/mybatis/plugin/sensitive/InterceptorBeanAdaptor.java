//package com.tangcheng.adapter.db.mybatis.plugin.sensitive;
//
//
//import com.tangcheng.adapter.db.mybatis.plugin.sensitive.service.AESBase64EncryptionAlgorithm;
//import org.apache.ibatis.plugin.Interceptor;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.springframework.beans.factory.InitializingBean;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import java.util.List;
//
//@Component
//public class InterceptorBeanAdaptor implements InitializingBean {
//
//    @Autowired
//    private SqlSessionFactory sqlSessionFactory;
//
//    @Autowired
//    private AESBase64EncryptionAlgorithm aesBase64EncryptionAlgorithm;
//
//    @Override
//    public void afterPropertiesSet() {
//        List<Interceptor> interceptors = sqlSessionFactory.getConfiguration().getInterceptors();
//        if (null == interceptors || interceptors.size() == 0) {
//            return;
//        }
//        for (Interceptor interceptor : interceptors) {
//            if (interceptor instanceof SensitiveFieldInterceptor) {
//                SensitiveFieldInterceptor sensitiveFieldInterceptor = (SensitiveFieldInterceptor) interceptor;
//                sensitiveFieldInterceptor.setEncryptionAlgorithm(aesBase64EncryptionAlgorithm);
//                sensitiveFieldInterceptor.setAesSecret(aesBase64EncryptionAlgorithm.getAesSecret());
//                break;
//            }
//        }
//    }
//
//}