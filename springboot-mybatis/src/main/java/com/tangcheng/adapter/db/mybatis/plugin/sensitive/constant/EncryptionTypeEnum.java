package com.tangcheng.adapter.db.mybatis.plugin.sensitive.constant;

public enum EncryptionTypeEnum {
    ENCRYPT,
    DECRYPT,
    ENDECRYPT,
    DECRYPT_PARAM;

    EncryptionTypeEnum() {
    }

}
