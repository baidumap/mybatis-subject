package com.tangcheng.adapter.db.user.dao.mapper;

import com.tangcheng.adapter.db.mybatis.plugin.sensitive.annotation.SensitiveField;
import com.tangcheng.adapter.db.mybatis.plugin.sensitive.annotation.SensitiveFieldEncryption;
import com.tangcheng.adapter.db.mybatis.plugin.sensitive.constant.EncryptionTypeEnum;
import com.tangcheng.adapter.db.user.dao.entity.SysAccount;
import com.tangcheng.adapter.db.user.dao.entity.SysAccountExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysAccountMapper {
    long countByExample(SysAccountExample example);

    int deleteByExample(SysAccountExample example);

    int deleteByPrimaryKey(Long id);

    @SensitiveFieldEncryption(EncryptionTypeEnum.ENCRYPT)
    int insert(@SensitiveField SysAccount record);

    int insertSelective(SysAccount record);

    @SensitiveFieldEncryption(EncryptionTypeEnum.ENDECRYPT)
    List<SysAccount> selectByExample(@SensitiveField SysAccountExample example);

    @SensitiveFieldEncryption(EncryptionTypeEnum.DECRYPT)
    SysAccount selectByPrimaryKey(Long id);

    @SensitiveFieldEncryption(EncryptionTypeEnum.ENCRYPT)
    int updateByExampleSelective(@SensitiveField @Param("record") SysAccount record, @SensitiveField @Param("example") SysAccountExample example);

    int updateByExample(@Param("record") SysAccount record, @Param("example") SysAccountExample example);

    int updateByPrimaryKeySelective(SysAccount record);

    int updateByPrimaryKey(SysAccount record);
}