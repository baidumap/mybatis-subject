package com.tangcheng.adapter.db.user.dao.entity;

import java.io.Serializable;
import java.util.Arrays;

public class SensitiveField implements Serializable {
    private Integer id;

    private String name;

    private String phone;

    private String phoneEncryptedBase64;

    private String secretKey;

    private byte[] phoneEncryptedBinary;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getPhoneEncryptedBase64() {
        return phoneEncryptedBase64;
    }

    public void setPhoneEncryptedBase64(String phoneEncryptedBase64) {
        this.phoneEncryptedBase64 = phoneEncryptedBase64 == null ? null : phoneEncryptedBase64.trim();
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey == null ? null : secretKey.trim();
    }

    public byte[] getPhoneEncryptedBinary() {
        return phoneEncryptedBinary;
    }

    public void setPhoneEncryptedBinary(byte[] phoneEncryptedBinary) {
        this.phoneEncryptedBinary = phoneEncryptedBinary;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SensitiveField other = (SensitiveField) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getPhoneEncryptedBase64() == null ? other.getPhoneEncryptedBase64() == null : this.getPhoneEncryptedBase64().equals(other.getPhoneEncryptedBase64()))
            && (this.getSecretKey() == null ? other.getSecretKey() == null : this.getSecretKey().equals(other.getSecretKey()))
            && (Arrays.equals(this.getPhoneEncryptedBinary(), other.getPhoneEncryptedBinary()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getPhoneEncryptedBase64() == null) ? 0 : getPhoneEncryptedBase64().hashCode());
        result = prime * result + ((getSecretKey() == null) ? 0 : getSecretKey().hashCode());
        result = prime * result + (Arrays.hashCode(getPhoneEncryptedBinary()));
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", phone=").append(phone);
        sb.append(", phoneEncryptedBase64=").append(phoneEncryptedBase64);
        sb.append(", secretKey=").append(secretKey);
        sb.append(", phoneEncryptedBinary=").append(phoneEncryptedBinary);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}