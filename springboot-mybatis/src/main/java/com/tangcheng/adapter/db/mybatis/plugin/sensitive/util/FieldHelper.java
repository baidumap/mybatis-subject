package com.tangcheng.adapter.db.mybatis.plugin.sensitive.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Pattern;

public class FieldHelper {


    public static boolean checkStringInCollection(Collection collection) {
        Iterator iterator = collection.iterator();
        Object o;
        do {
            if (!iterator.hasNext()) {
                return true;
            }
            o = iterator.next();
        } while (o instanceof String);
        return false;
    }

    public static boolean checkIfObject(String type) {
        String[] types = new String[]{"int", "float", "boolean", "double", "char", "short", "byte", "long", "java.lang.String", "java.util.Date", "java.math.BigInteger", "java.math.BigDecimal", "java.lang.Integer", "java.sql.Time", "java.lang.Long", "java.lang.Double", "java.lang.Float", "java.lang.Boolean", "java.lang.Short"};
        for (String val : types) {
            if (val.equals(type)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isBase64(String str) {
        String base64Pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
        return Pattern.matches(base64Pattern, str);
    }

}