package com.tangcheng.adapter.db.mybatis.plugin.sensitive.constant;

public enum EncryptionAlgorithmEnum {
    CUSTOMIZE,
    AES_BASE64,
    KMS,
    RSA,
    AUTO;

    EncryptionAlgorithmEnum() {
    }

}
