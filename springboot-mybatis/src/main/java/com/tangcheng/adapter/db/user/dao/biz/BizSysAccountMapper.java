package com.tangcheng.adapter.db.user.dao.biz;

import com.tangcheng.adapter.db.user.dao.mapper.SysAccountMapper;

/**
 * @Auther: cheng.tang
 * @Date: 2024/8/13
 * @Description: mybatis-subject
 */
public interface BizSysAccountMapper extends SysAccountMapper {

}
