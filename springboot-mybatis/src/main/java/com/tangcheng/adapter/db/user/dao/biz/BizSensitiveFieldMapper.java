package com.tangcheng.adapter.db.user.dao.biz;

import com.tangcheng.adapter.db.user.dao.mapper.SensitiveFieldMapper;

/**
 * @Auther: cheng.tang
 * @Date: 2024/8/15
 * @Description: mybatis-subject
 */
public interface BizSensitiveFieldMapper extends SensitiveFieldMapper {

}
