package com.tangcheng.adapter.db.user.dao.mapper;

import com.tangcheng.adapter.db.user.dao.entity.SensitiveField;
import com.tangcheng.adapter.db.user.dao.entity.SensitiveFieldExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SensitiveFieldMapper {
    long countByExample(SensitiveFieldExample example);

    int deleteByExample(SensitiveFieldExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SensitiveField record);

    int insertSelective(SensitiveField record);

    List<SensitiveField> selectByExampleWithBLOBs(SensitiveFieldExample example);

    List<SensitiveField> selectByExample(SensitiveFieldExample example);

    SensitiveField selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SensitiveField record, @Param("example") SensitiveFieldExample example);

    int updateByExampleWithBLOBs(@Param("record") SensitiveField record, @Param("example") SensitiveFieldExample example);

    int updateByExample(@Param("record") SensitiveField record, @Param("example") SensitiveFieldExample example);

    int updateByPrimaryKeySelective(SensitiveField record);

    int updateByPrimaryKeyWithBLOBs(SensitiveField record);

    int updateByPrimaryKey(SensitiveField record);
}