package com.tangcheng.adapter.db.mybatis.plugin.sensitive.service;

public interface IEncryptionAlgorithm {

    String getAesSecret();

    String encrypt(String content);

    String decrypt(String content);

}