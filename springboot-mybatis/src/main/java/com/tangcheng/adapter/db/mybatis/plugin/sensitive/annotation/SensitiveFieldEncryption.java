package com.tangcheng.adapter.db.mybatis.plugin.sensitive.annotation;


import com.tangcheng.adapter.db.mybatis.plugin.sensitive.constant.EncryptionAlgorithmEnum;
import com.tangcheng.adapter.db.mybatis.plugin.sensitive.constant.EncryptionTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface SensitiveFieldEncryption {

    EncryptionTypeEnum value();

    EncryptionAlgorithmEnum method() default EncryptionAlgorithmEnum.AUTO;

}