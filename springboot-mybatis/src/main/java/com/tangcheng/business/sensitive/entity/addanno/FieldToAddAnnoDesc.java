package com.tangcheng.business.sensitive.entity.addanno;

import lombok.Data;

import java.util.Set;

@Data
public class FieldToAddAnnoDesc {
    //类的全限定名
    private String className;
    //属性名
    private Set<String> fieldNames;
}
