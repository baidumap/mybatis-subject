package com.tangcheng.business.sensitive.entity.exportdo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain=true)
public class ExampleUsageDO {
    @Excel(name="表名")
    private String databaseName;
    @Excel(name="exmaple名")
    private String exampleName;
    @Excel(name="类名")
    private String className;
    @Excel(name="行号")
    private Integer line;
}
