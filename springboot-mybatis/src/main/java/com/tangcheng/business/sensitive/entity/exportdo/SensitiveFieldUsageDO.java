package com.tangcheng.business.sensitive.entity.exportdo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SensitiveFieldUsageDO {
    @Excel(name="表名")
    private String tableName;
    @Excel(name="字段名")
    private String sensitiveFieldName;
    @Excel(name="接口名")
    private String interfaceName;
    @Excel(name="方法名")
    private String methodName;
    @Excel(name="加密类型")
    private String encryptionType;
    @Excel(name="加密注解")
    private String encryptionAnno;
}
