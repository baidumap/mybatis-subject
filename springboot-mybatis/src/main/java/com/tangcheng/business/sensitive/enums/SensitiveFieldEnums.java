package com.tangcheng.business.sensitive.enums;


import com.google.common.collect.Lists;

import java.util.List;

public enum SensitiveFieldEnums {
    Table1("sensitive_field", "phone", "phone_encrypted_base64");
    private String tableName;
    private List<String> fieldsName;

    public String getTableName() {
        return tableName;
    }

    public List<String> getFields() {
        return fieldsName;
    }

    private SensitiveFieldEnums(String tableName, String... fieldsName) {
        this.tableName = tableName;
        this.fieldsName = Lists.newArrayList(fieldsName);
    }
}
