package com.tangcheng.business.sensitive.entity.addanno;

import com.tangcheng.business.sensitive.enums.MethodTargetAnnoEnum;
import lombok.Data;

@Data
public class MethodToAddAnnoDesc {
    //方法名
    private String methodName;
    //注解类型
    private MethodTargetAnnoEnum annoType;
}
