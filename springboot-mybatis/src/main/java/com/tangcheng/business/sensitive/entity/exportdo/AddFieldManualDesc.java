package com.tangcheng.business.sensitive.entity.exportdo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Set;

@Data
public class AddFieldManualDesc {
    @Excel(name = "类的全限定名")
    private String className;
    @Excel(name="需要加注解的字段")
    private Set<String> fieldSet;
    @Excel(name="字段数量")
    private Integer size;
}
