package com.tangcheng.business.sensitive.entity.exportdo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SensitiveFieldUsageInControllerDO {
    @Excel(name="表名")
    private String tableName;
    @Excel(name="字段名")
    private String sensitiveFieldName;
    @Excel(name="接口名")
    private String controllerName;
    @Excel(name="方法名")
    private String methodName;
    @Excel(name="访问路径")
    private String accessPath;
    @Excel(name="加密类型")
    private String encryptionType;
}
