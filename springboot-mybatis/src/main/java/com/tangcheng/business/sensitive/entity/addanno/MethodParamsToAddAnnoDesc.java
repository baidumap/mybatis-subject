package com.tangcheng.business.sensitive.entity.addanno;

import lombok.Data;

import java.util.Set;

/**
 * 用于描述需要添加@SensitiveField注解的参数列表
 */
@Data
public class MethodParamsToAddAnnoDesc {
    //方法名
    private String methodName;
    //参数索引
    private Set<Integer> indexs; 
}
