package com.tangcheng.business.sensitive;

import com.google.common.collect.Lists;
import com.tangcheng.business.sensitive.entity.exportdo.ExampleUsageDO;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static com.tangcheng.business.sensitive.DataMaskCommonUtils.getExampleNameFromTableName;

/**
 * 获取所有涉及加密的表的所有Example的usages
 * 目前没有用到，example都需要处理
 */
@Slf4j
public class SearchExampleUsageUtil {

    private static List<String> tableNames = Lists.newArrayList("users", "sys_role", "sys_resource");

    public static void main(String[] args) throws FileNotFoundException {
        Path path = Paths.get("src/main/java");
        FileOutputStream fileOutputStream = new FileOutputStream("data-masking.xls");
        try {
            List<ExampleUsageDO> exampleUsageDOS = new ArrayList<>();
            for (String tableName : tableNames) {
                System.out.println("current table is " + tableName);
                //fileOutputStream.write(String.format("%s usages===============================\n",exampleName).getBytes());
                //System.out.println(exampleName+"'s usages=======================================");
                Files.walkFileTree(path, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if (file.toFile().getName().contains("SearchExampleUsageUtil")) {
                            return FileVisitResult.CONTINUE;
                        }
                        try (LineNumberReader lineNumberReader = new LineNumberReader(new FileReader(file.toFile()))) {
                            String line;
                            while ((line = lineNumberReader.readLine()) != null) {
                                if (line.contains(getExampleNameFromTableName(tableName))) {
                                    //System.out.println(file.getFileName().toString().split("\\.")[0]+","+"Line "+lineNumberReader.getLineNumber());
                                    //fileOutputStream.write(String.format(file.getFileName().toString().split("\\.")[0]+",Line %s\n",lineNumberReader.getLineNumber()).getBytes());
                                    ExampleUsageDO exampleUsageDO = new ExampleUsageDO()
                                            .setDatabaseName(tableName)
                                            .setExampleName(getExampleNameFromTableName(tableName))
                                            .setClassName(file.toFile().getName().split("\\.")[0])
                                            .setLine(lineNumberReader.getLineNumber());
                                    exampleUsageDOS.add(exampleUsageDO);
                                }
                            }
                        } catch (Exception e) {
                            log.warn("error:", e);
                        }
                        return FileVisitResult.CONTINUE;
                    }
                });

            }

            DataMaskCommonUtils.exportExcel(exampleUsageDOS, "exmaples的usages", "User-Example-Usages", fileOutputStream, ExampleUsageDO.class);
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
