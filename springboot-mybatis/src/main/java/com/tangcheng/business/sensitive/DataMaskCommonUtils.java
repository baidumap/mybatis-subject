package com.tangcheng.business.sensitive;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.OutputStream;
import java.util.List;

@Slf4j
public class DataMaskCommonUtils {

    /**
     * 导出excel报表
     *
     * @param exportList
     * @param title
     * @param sheetName
     * @param outputStream
     * @param clazz
     */
    public static void exportExcel(List<?> exportList, String title, String sheetName, OutputStream outputStream, Class<?> clazz) {
        try {
            ExportParams exportParams = new ExportParams(title, sheetName);

            Workbook workbook = ExcelExportUtil.exportExcel(exportParams, clazz, exportList);

            workbook.write(outputStream);
        } catch (Exception e) {
            log.info("export excel error:", e);
        }
    }

    /**
     * 下划线格式转驼峰
     *
     * @param field
     * @return
     */
    public static String humpConv(String field) {
        char[] charArray = field.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int idx = 0; idx < charArray.length - 1; idx++) {
            if (charArray[idx] == '_') {
                sb.append(Character.toUpperCase(charArray[idx + 1]));
                idx++;
            } else {
                sb.append(charArray[idx]);
            }
        }
        sb.append(charArray[charArray.length - 1]);
        return sb.toString();
    }

    /**
     * 下划线转Example名
     *
     * @param tableName
     * @return
     */
    public static String getExampleNameFromTableName(String tableName) {
        return buildFileNameBy("", tableName, "Example");
    }

    public static String getMapperName(String tableName) {
        return getMapperName(tableName, "Mapper");
    }

    public static String getMapperName(String prefix, String tableName) {
        return buildFileNameBy(prefix, tableName, "Mapper");
    }

    public static String buildFileNameBy(String prefix, String tableName, String suffix) {
        char[] charArray = tableName.toCharArray();
        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank(prefix)) {
            sb.append(prefix);
        }
        sb.append(Character.toUpperCase(charArray[0]));
        for (int idx = 1; idx < charArray.length - 1; idx++) {
            if (charArray[idx] == '_') {
                sb.append(Character.toUpperCase(charArray[idx + 1]));
                idx++;
            } else {
                sb.append(charArray[idx]);
            }
        }
        sb.append(charArray[charArray.length - 1]);
        if (StringUtils.isNotBlank(suffix)) {
            sb.append(suffix);
        }
        return sb.toString();
    }

}

