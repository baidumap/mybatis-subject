- SSLHandshakeException Q:

```java
Caused by:javax.net.ssl.SSLHandshakeException:No appropriate protocol(protocol is disabled or cipher suites are inappropriate)
```

A:
在jdbc连接上添加

```properties
useSSL=false
```

- MySQLNonTransientConnectionException Q:

```java
org.apache.ibatis.exceptions.PersistenceException:
        ### Error querying database.Cause:com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException:Could not create connection to database server.
        ### The error may exist in mapper/CountryMapper.xml
        ### The error may involve mapper.CountryMapper.xml.selectAll
        ### The error occurred while executing a query
        ### Cause:com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException:Could not create connection to database server.

```

A:

1. 升级应用的MySQL驱动为8+。本次使用了最新

```xml

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.23</version>
</dependency>
```

2. 更新与mysql-connector驱动匹配的driver为

```java
com.mysql.cj.jdbc.Driver
```

3. 增加时区MySQL url上增加时区配置。MySQL 8+驱动的默认时间会比中间晚8个小时

```java
jdbc:mysql://localhost:3306/tqcs?useSSL=false&amp;serverTimezone=Asia/Shanghai
```

Q:

```java
org.apache.ibatis.exceptions.PersistenceException:
        ### Error querying database.Cause:java.sql.SQLException:Error setting driver on UnpooledDataSource.Cause:java.lang.ClassNotFoundException:Cannot find class:com.mysql.cj.jdbc.Driver
        ### The error may exist in mapper/CountryMapper.xml
        ### The error may involve mapper.CountryMapper.xml.selectAll
        ### The error occurred while executing a query
        ### Cause:java.sql.SQLException:Error setting driver on UnpooledDataSource.Cause:java.lang.ClassNotFoundException:Cannot find class:com.mysql.cj.jdbc.Driver

```

http://zsy.coolthhink.cn/
A:
新的MySQL驱动Jar在项目中还没有更新完成。测试用例还没有识别到。 需要在Dependency Analyzer确定已经是最新的依赖才可。

```xml

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>8.0.23</version>
</dependency>
```

- org.apache.ibatis.binding.BindingException Q:

```java
org.apache.ibatis.binding.BindingException:Invalid bound statement(not found):com.tangcheng.mybatis.introduction.rbac.mapper.SysAccountDOMapper.selectByPrimaryKey
```

A:

```java
/**
 * org.apache.ibatis.binding.MapperMethod.SqlCommand.SqlCommand
 *org.apache.ibatis.binding.MapperMethod.java中的内部类SqlCommand
 */
public static class SqlCommand {

    private final String name;
    private final SqlCommandType type;

    /**
     * 如果找不到对应的SQL，就报错
     *  Invalid bound statement (not found):
     * @param configuration
     * @param mapperInterface
     * @param method
     */
    public SqlCommand(Configuration configuration, Class<?> mapperInterface, Method method) {
        final String methodName = method.getName();
        final Class<?> declaringClass = method.getDeclaringClass();
        MappedStatement ms = resolveMappedStatement(mapperInterface, methodName, declaringClass,
                configuration);
        if (ms == null) {
            if (method.getAnnotation(Flush.class) != null) {
                name = null;
                type = SqlCommandType.FLUSH;
            } else {
                throw new BindingException("Invalid bound statement (not found): "
                        + mapperInterface.getName() + "." + methodName);
            }
        } else {
            name = ms.getId();
            type = ms.getSqlCommandType();
            if (type == SqlCommandType.UNKNOWN) {
                throw new BindingException("Unknown execution method for: " + name);
            }
        }
    }

    /**
     * 使用Mapper.java的全路径+方法名，作为statementId去找对应的SQL
     * @param mapperInterface
     * @param methodName
     * @param declaringClass
     * @param configuration
     * @return
     */
    private MappedStatement resolveMappedStatement(Class<?> mapperInterface, String methodName,
                                                   Class<?> declaringClass, Configuration configuration) {
        String statementId = mapperInterface.getName() + "." + methodName;
        if (configuration.hasStatement(statementId)) {
            return configuration.getMappedStatement(statementId);
        } else if (mapperInterface.equals(declaringClass)) {
            return null;
        }
        for (Class<?> superInterface : mapperInterface.getInterfaces()) {
            if (declaringClass.isAssignableFrom(superInterface)) {
                MappedStatement ms = resolveMappedStatement(superInterface, methodName,
                        declaringClass, configuration);
                if (ms != null) {
                    return ms;
                }
            }
        }
        return null;
    }

}
```

- org.apache.ibatis.binding.BindingException: Type interface Mappper接口名 is not known to the MapperRegistry. Q:
  configuration.mappers.package配置错误。应该配置Mapper接口所在的package

```java
org.apache.ibatis.binding.BindingException:Type

interface com.tangcheng.mybatis.introduction.rbac.mapper.SysAccountDOMapper is not known to the MapperRegistry.
```

A:  
mybatis-config.xml中configuration.mappers下配置package时，配置的是Mapper接口所在的package名

```xml

<mappers>
    <package name="mapper.rbac"/>
</mappers>
```

解析```java configuration.mappers.package```配置的代码如下：

```java
org.apache.ibatis.io.ResolverUtil.find
/**
 * Scans for classes starting at the package provided and descending into subpackages.
 * Each class is offered up to the Test as it is discovered, and if the Test returns
 * true the class is retained.  Accumulated classes can be fetched by calling
 * {@link #getClasses()}.
 *
 * @param test
 *          an instance of {@link Test} that will be used to filter classes
 * @param packageName
 *          the name of the package from which to start scanning for classes, e.g. {@code net.sourceforge.stripes}
 * @return the resolver util
 */
public ResolverUtil<T> find(Test test,String packageName){
        String path=getPackagePath(packageName);

        try{
        List<String> children=VFS.getInstance().list(path);
        for(String child:children){
        if(child.endsWith(".class")){
        addIfMatching(test,child);
        }
        }
        }catch(IOException ioe){
        log.error("Could not read package: "+packageName,ioe);
        }

        return this;
        }
```
