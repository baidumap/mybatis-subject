package com.tangcheng.user.business.user.service;

import com.tangcheng.user.adapter.repository.user.po.RmUserPO;
import com.tangcheng.user.adapter.repository.user.service.RmUserPOService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @Auther: cheng.tang
 * @Date: 2023/10/2
 * @Description: mybatis-subject
 */
@Service
@Slf4j
public class RememberMeDetailsService implements UserDetailsService {

    @Autowired
    private RmUserPOService rmUserPOService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        RmUserPO user = rmUserPOService.findByUsername(username);
        if (user == null) {
            log.warn(" UsernameNotFoundException {} ", username);
            throw new UsernameNotFoundException("User not found");
        }
        return User.withUsername(user.getUsername())
                .password(user.getPassword()).roles("USER").build();
    }


}
