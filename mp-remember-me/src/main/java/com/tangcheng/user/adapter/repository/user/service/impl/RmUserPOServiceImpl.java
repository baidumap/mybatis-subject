package com.tangcheng.user.adapter.repository.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tangcheng.user.adapter.repository.user.po.RmUserPO;
import com.tangcheng.user.adapter.repository.user.service.RmUserPOService;
import com.tangcheng.user.adapter.repository.user.mapper.RmUserPOMapper;
import org.springframework.stereotype.Service;

/**
 * @author cheng.tang
 * @description 针对表【rm_user】的数据库操作Service实现
 * @createDate 2023-10-02 17:25:33
 */
@Service
public class RmUserPOServiceImpl extends ServiceImpl<RmUserPOMapper, RmUserPO>
        implements RmUserPOService {

    @Override
    public RmUserPO findByUsername(String username) {
        LambdaQueryWrapper<RmUserPO> lambdaQueryWrapper = Wrappers.lambdaQuery();
        lambdaQueryWrapper.eq(RmUserPO::getUsername, username);
        lambdaQueryWrapper.last(" limit 1 ");
        return getOne(lambdaQueryWrapper);
    }


}




