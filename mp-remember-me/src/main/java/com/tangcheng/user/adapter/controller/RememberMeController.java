package com.tangcheng.user.adapter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Auther: cheng.tang
 * @Date: 2023/10/2
 * @Description: mybatis-subject
 */
@Controller
@RequestMapping("/remberme")
public class RememberMeController {

    @GetMapping("/login")
    public String showLoginPage() {
        return "/rember-me/login";
    }

    @GetMapping("/profile")
    public String showUserProfilePage() {
        return "/rember-me/profile";
    }


}
