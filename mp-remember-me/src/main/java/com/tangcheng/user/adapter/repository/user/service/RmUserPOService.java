package com.tangcheng.user.adapter.repository.user.service;

import com.tangcheng.user.adapter.repository.user.po.RmUserPO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author cheng.tang
* @description 针对表【rm_user】的数据库操作Service
* @createDate 2023-10-02 17:25:33
*/
public interface RmUserPOService extends IService<RmUserPO> {

    RmUserPO findByUsername(String username);




}
