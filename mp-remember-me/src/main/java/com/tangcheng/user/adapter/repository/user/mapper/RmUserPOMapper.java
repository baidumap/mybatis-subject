package com.tangcheng.user.adapter.repository.user.mapper;

import com.tangcheng.user.adapter.repository.user.po.RmUserPO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author cheng.tang
* @description 针对表【rm_user】的数据库操作Mapper
* @createDate 2023-10-02 17:25:33
* @Entity com.tangcheng.user.adapter.repository.user.po.RmUserPO
*/
public interface RmUserPOMapper extends BaseMapper<RmUserPO> {

}




