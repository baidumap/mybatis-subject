package com.tangcheng.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Auther: cheng.tang
 * @Date: 2023/10/2
 * @Description: mybatis-subject
 */
@MapperScan("com.tangcheng.user.adapter.repository.*.mapper")
@SpringBootApplication
public class MpRememberMeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MpRememberMeApplication.class, args);
    }

}
