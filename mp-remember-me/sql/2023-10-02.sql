CREATE TABLE `rm_user`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `username`       varchar(50)  NOT NULL,
    `password`       varchar(100) NOT NULL,
    `remember_token` varchar(100) DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `rm_user` (`id`, `username`, `password`, `remember_token`)
VALUES (1000, 'admin', '$2a$10$qe8ZU7tfy2Z67g32m/bWvuipJNJTKW/3zo0o6IHBNykLrwhxJFVrG', '96b9b6de-e28d-4abf-97e2-f92fe577b525');


