### 一些操作要点

##### mybatis配置日志输出

要指定mapper.xml中配置的namespace节点中的路径。 譬如CountryMapper.xml中SQL中执行日志，要配置。目前项目中是没有mapper.area或mapper的java代码package路径的

```properties
log4j.logger.mapper.area=trace
或
log4j.logger.mapper=trace
```

