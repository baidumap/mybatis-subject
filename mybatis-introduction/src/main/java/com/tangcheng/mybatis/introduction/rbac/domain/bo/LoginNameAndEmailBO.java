package com.tangcheng.mybatis.introduction.rbac.domain.bo;

import lombok.Builder;
import lombok.Data;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/5/22 11:26 PM
 * @Description:
 */
@Builder
@Data
public class LoginNameAndEmailBO {
    /**
     * 登陆名
     */
    private String loginName;

    /**
     * 邮箱
     */
    private String email;

}
