package com.tangcheng.mybatis.introduction.area.mapper;

import com.tangcheng.mybatis.introduction.BaseMapperTest;
import com.tangcheng.mybatis.introduction.area.domain.entity.Country;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * Created by tangcheng on 7/16/2017.
 */
@Slf4j
public class CountryMapperTest extends BaseMapperTest {

    @Test
    public void testSelectAll() {
        SqlSession sqlSession = sqlSessionFactory.openSession();
        try {
            List<Country> countryList = sqlSession.selectList("selectAllCountry");
            for (Country country : countryList) {
                Long id = country.getId();
                String name = country.getName();
                String code = country.getCode();
                log.info("{},{},{}", id, name, code);
            }
        } finally {
            sqlSession.close();
        }
    }

}