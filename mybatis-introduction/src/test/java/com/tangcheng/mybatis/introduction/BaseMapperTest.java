package com.tangcheng.mybatis.introduction;

import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;

import java.io.IOException;
import java.io.Reader;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/5/5 7:26 AM
 * @Description:
 */
@Slf4j
public class BaseMapperTest {

    protected static SqlSessionFactory sqlSessionFactory;

    @BeforeClass
    public static void init() {
        try {
            Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }


}
