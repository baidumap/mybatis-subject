package com.tangcheng.mybatis.introduction.rbac.mapper;

import com.tangcheng.mybatis.introduction.BaseMapperTest;
import com.tangcheng.mybatis.introduction.rbac.domain.bo.LoginNameAndEmailBO;
import com.tangcheng.mybatis.introduction.rbac.domain.entity.SysAccountDO;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class SysAccountDOMapperTest extends BaseMapperTest {

    @Test
    public void testSelectById() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysAccountDOMapper sysAccountDOMapper = sqlSession.getMapper(SysAccountDOMapper.class);
            SysAccountDO sysAccountDO = sysAccountDOMapper.selectByPrimaryKey(1L);
            log.info("{} ", sysAccountDO);
            Assert.assertEquals("admin", sysAccountDO.getLoginName());
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void test_selectAll() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysAccountDOMapper sysAccountDOMapper = sqlSession.getMapper(SysAccountDOMapper.class);
            List<SysAccountDO> sysAccountDOList = sysAccountDOMapper.selectAll();
            log.info("{} ", sysAccountDOList);
            assertThat(sysAccountDOList.size()).isEqualTo(2);
        } finally {
            sqlSession.close();
        }
    }


    @Test
    public void test_selectByIdList() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysAccountDOMapper sysAccountDOMapper = sqlSession.getMapper(SysAccountDOMapper.class);
            List<SysAccountDO> sysAccountDOList = sysAccountDOMapper.selectByIdList(Arrays.asList(1L, 2L));
            log.info("{} ", sysAccountDOList);
            assertThat(sysAccountDOList.size()).isEqualTo(2);
        } finally {
            sqlSession.close();
        }
    }


    @Test
    public void test_selectByLoginNameAndEmailList() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysAccountDOMapper sysAccountDOMapper = sqlSession.getMapper(SysAccountDOMapper.class);
            List<LoginNameAndEmailBO> loginNameAndEmailList = new ArrayList<LoginNameAndEmailBO>();
            loginNameAndEmailList.add(LoginNameAndEmailBO.builder().loginName("admin").email("admin@chaojihao.net").build());
            loginNameAndEmailList.add(LoginNameAndEmailBO.builder().loginName("test").email("test@chaojihao.net").build());
            List<SysAccountDO> sysAccountDOList = sysAccountDOMapper.selectByLoginNameAndEmailList(loginNameAndEmailList);
            log.info("{} ", sysAccountDOList);
            assertThat(sysAccountDOList.size()).isEqualTo(2);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void test_selectAllLikeMatchedAccountIds() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysAccountDOMapper sysAccountDOMapper = sqlSession.getMapper(SysAccountDOMapper.class);
            List<Long> sysAccountIdList = sysAccountDOMapper.selectAllLikeMatchedAccountIds(Arrays.asList("管理", "普通", "用户"));
            log.info("{} ", sysAccountIdList);
            assertThat(sysAccountIdList.size()).isEqualTo(2);
        } finally {
            sqlSession.close();
        }
    }

    @Test
    public void test_batchInsert() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysAccountDOMapper sysAccountDOMapper = sqlSession.getMapper(SysAccountDOMapper.class);
            List<SysAccountDO> accountList = new ArrayList<SysAccountDO>();
            int expectedInsertRecordCount = 10;
            for (int i = 0; i < expectedInsertRecordCount; i++) {
                SysAccountDO sysAccountDO = new SysAccountDO();
                sysAccountDO.setNickName("用户_" + i);
                sysAccountDO.setLoginName("user_" + i);
                sysAccountDO.setPwd("pwd_" + i);
                sysAccountDO.setEmail("email_" + i);
                sysAccountDO.setCreatorBy(1L);
                sysAccountDO.setModifiedBy(1L);
                accountList.add(sysAccountDO);
            }
            int actualInsertRecordsCount = sysAccountDOMapper.batchInsert(accountList);
            assertThat(actualInsertRecordsCount).isEqualTo(expectedInsertRecordCount);
        } finally {
            sqlSession.rollback(true);
            sqlSession.close();
        }
    }

    @Test
    public void test_updateByMap() {
        SqlSession sqlSession = getSqlSession();
        try {
            SysAccountDOMapper sysAccountDOMapper = sqlSession.getMapper(SysAccountDOMapper.class);
            Map<String, Object> mapParams = new HashMap<String, Object>();
            mapParams.put("id", 1L);
            mapParams.put("nick_name", "newNickName");
            mapParams.put("avatar", "https://f.chaojihao.net/logo/wxmplogog.jpeg");
            int actualInsertRecordsCount = sysAccountDOMapper.updateByMap(mapParams);
            assertThat(actualInsertRecordsCount).isEqualTo(1);
        } finally {
            sqlSession.rollback(true);
            sqlSession.close();
        }
    }

}