/*
Source Server         : localhost
Source Server Version : 100113
Source Host           : localhost:3306
Source Database       : mybatis-subject

Target Server Type    : MYSQL
Target Server Version : 100113
File Encoding         : 65001

Date: 2017-07-17 12:50:57
*/
CREATE DATABASE `tqcs` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `tqcs`;

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for country
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country`
(
    `id`   bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(255)        NOT NULL,
    `code` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country`
VALUES ('1', '中国', 'CN');
INSERT INTO `country`
VALUES ('2', '美国', 'US');
INSERT INTO `country`
VALUES ('3', '俄罗斯', 'RU');
INSERT INTO `country`
VALUES ('4', '英国', 'GB');
INSERT INTO `country`
VALUES ('5', '法国', 'FR');

DROP TABLE IF EXISTS `sys_account`;
CREATE TABLE `sys_account`
(
    `id`           bigint unsigned NOT NULL AUTO_INCREMENT,
    `nick_name`    varchar(45)     NOT NULL COMMENT '呢称',
    `login_name`   varchar(45)     NOT NULL COMMENT '登陆用户名',
    `pwd`          varchar(45)     NOT NULL COMMENT '密码',
    `email`        varchar(45)     NOT NULL COMMENT '邮箱',
    `avatar`       varchar(45) DEFAULT NULL,
    `profile`      varchar(45) DEFAULT NULL,
    `gmt_create`   datetime    DEFAULT CURRENT_TIMESTAMP,
    `creator_by`   bigint          NOT NULL,
    `gmt_modified` datetime    DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间，默认取服务器当前时间。有更新操作时触发',
    `modified_by`  bigint          NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `sys_account_role`;
CREATE TABLE `sys_account_role`
(
    `id`         bigint          NOT NULL AUTO_INCREMENT COMMENT '用户角色关联表',
    `account_id` bigint unsigned NOT NULL,
    `role_id`    bigint unsigned NOT NULL,
    `creator_by` bigint unsigned NOT NULL,
    `gmt_create` datetime        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`
(
    `id`           bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`         varchar(45)     NOT NULL COMMENT '资源名称',
    `type`         varchar(45)     NOT NULL COMMENT '资源类型',
    `url`          varchar(45)              DEFAULT NULL COMMENT '资源URL',
    `gmt_create`   datetime        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `creator_by`   bigint          NOT NULL,
    `gmt_modified` datetime                 DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `modified_by`  bigint          NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `id`           bigint unsigned  NOT NULL AUTO_INCREMENT,
    `name`         varchar(45)      NOT NULL,
    `enabled`      tinyint unsigned NOT NULL,
    `creator_by`   bigint           NOT NULL,
    `gmt_create`   datetime DEFAULT CURRENT_TIMESTAMP,
    `modified_By`  bigint           NOT NULL,
    `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '1:可用。0：不可用',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource`
(
    `id`          bigint unsigned NOT NULL AUTO_INCREMENT,
    ` role_id`    bigint unsigned NOT NULL,
    `resource_id` bigint unsigned NOT NULL,
    `gmt_create`  datetime        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `creator_by`  bigint unsigned NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `metadata_data_dict`;
CREATE TABLE `metadata_data_dict`
(
    `id`                           bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'primary key',
    `gmt_create`                   datetime            NOT NULL COMMENT 'create time',
    `gmt_modified`                 datetime            NOT NULL COMMENT 'modify time',
    `creator_id`                   bigint(20)          NOT NULL COMMENT '创建人id',
    `modifier_id`                  bigint(20)          NOT NULL COMMENT '修改人id',
    `deleted`                      tinyint(4)          NOT NULL DEFAULT '0' COMMENT '1:已删除 0：未删除',
    `metadata_type`                varchar(100)        NOT NULL COMMENT '业务枚举类型',
    `metadata_type_display`        varchar(100)        NOT NULL COMMENT '业务枚举类型的描述信息',
    `metadata_type_value`          varchar(100)        NOT NULL COMMENT '业务枚举类型中的一个取值',
    `metadata_type_value_display`  varchar(100)        NOT NULL COMMENT '业务枚举类型中一个取值的描述信息',
    `metadata_type_value_data_ext` varchar(100)                 DEFAULT '' COMMENT 'type-value组合下配置一个值，扩展支持简单的全局配置场景',
    `metadata_type_value_order`    int(11)             NOT NULL DEFAULT '0' COMMENT '一类枚举有多个值时的排序字段。数字越小越靠前',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uk_metadatatype_metadatatypevalue` (`metadata_type`, `metadata_type_value`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='元数据的数据字典';
