-- mysql中的boolean类型的数据，bit(1)和tinyint(1)哪个数据类型合适:
-- 在MySQL中，boolean类型的数据可以使用bit(1)和tinyint(1)来表示。这两个数据类型都可以用于存储逻辑值（0或1），但它们的用法和性能有一些差异。
-- 使用bit(1)类型时，它只使用1位来存储逻辑值，因此非常节省空间。然而，在进行比较时，需要使用二进制表示法（例如，使用BINARY关键字）。这可能会使查询语句稍微复杂一些。
-- 使用tinyint(1)类型时，它使用8位来存储整数值，比bit(1)类型多7位。虽然它占用的空间较多，但比较时可以使用标准的整数比较方式，不需要使用二进制表示法。
-- 如果你的数据只有0或1两种可能值，并且空间占用是一个关键问题，那么bit(1)类型可能是更好的选择。
-- 然而，如果你的查询中需要比较多于两个值的逻辑值，或者你希望使用标准的整数比较方式，那么tinyint(1)类型可能更适合你的需求。
-- 总结起来，选择哪种数据类型取决于你的具体需求和使用场景。如果你只需要存储0或1两个逻辑值，并且空间占用是一个关键问题，那么bit(1)类型是合适的。
-- 如果你需要比较多于两个值的逻辑值或者希望使用标准的整数比较方式，那么tinyint(1)类型更适合。

CREATE TABLE `task`
(
    `id`           bigint unsigned NOT NULL AUTO_INCREMENT,
    `name`         varchar(100) NOT NULL COMMENT '任务名称',
    `deleted`      tinyint(1) NOT NULL COMMENT '1:已删除 0未删除',
    `gmt_create`   datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `create_by`    varchar(45)  NOT NULL DEFAULT '' COMMENT '创建人帐户信息',
    `gmt_modified` datetime     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `modified_by`  varchar(45)  NOT NULL DEFAULT '' COMMENT '修改人帐户信息',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci COMMENT ='现实中的任务对应的结构化数据'