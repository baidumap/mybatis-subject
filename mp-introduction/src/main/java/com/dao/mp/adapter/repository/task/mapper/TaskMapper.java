package com.dao.mp.adapter.repository.task.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dao.mp.adapter.repository.task.domain.entity.Task;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/7/16 8:44 PM
 * @Description:
 */
public interface TaskMapper extends BaseMapper<Task> {
}
