package com.dao.mp.adapter.repository.global.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/7/16 9:31 PM
 * @Description:
 */
@Slf4j
@Component
public class MPAutoFillMetaObject implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        /**
         * fieldName 是java对象的名字
         */
        setFieldValByName("deleted", false, metaObject);
        Date now = new Date();
        setFieldValByName("gmtCreate", now, metaObject);
        setFieldValByName("gmtModified", now, metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Date now = new Date();
        setFieldValByName("gmtModified", now, metaObject);
    }

}
