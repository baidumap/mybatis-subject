package com.dao.mp.adapter.repository.global.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/7/17 11:53 AM
 * @Description:
 */
@Data
public class BaseDO {

    @TableId(type = IdType.AUTO)
    protected Long id;

    @TableField(value = "gmt_create", fill = FieldFill.INSERT)
    protected Date gmtCreate;
    @TableField(value = "gmt_modified", fill = FieldFill.INSERT_UPDATE)
    protected Date gmtModified;
    @TableField(value = "create_by")
    private String createBy;
    @TableField(value = "modified_by")
    private String modifiedBy;

    @TableField(value = "deleted", fill = FieldFill.INSERT)
    @TableLogic(value = "false", delval = "true")
    private Boolean deleted;

}
