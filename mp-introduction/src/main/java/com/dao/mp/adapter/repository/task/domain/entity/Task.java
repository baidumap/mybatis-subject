package com.dao.mp.adapter.repository.task.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.dao.mp.adapter.repository.global.domain.entity.BaseDO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/7/16 8:00 PM
 * @Description:
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("task")
public class Task extends BaseDO {

    @TableField("name")
    private String name;

}
