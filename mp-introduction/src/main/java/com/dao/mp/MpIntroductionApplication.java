package com.dao.mp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.dao.mp.adapter.repository.*.mapper")
@SpringBootApplication
public class MpIntroductionApplication {

    public static void main(String[] args) {
        SpringApplication.run(MpIntroductionApplication.class, args);
    }

}
