package com.dao.mp.business.task.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.dao.mp.adapter.repository.task.domain.entity.Task;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/8/16 9:47 AM
 * @Description:
 */
public interface TaskService extends IService<Task> {

}
