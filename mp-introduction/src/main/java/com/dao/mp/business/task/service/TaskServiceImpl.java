package com.dao.mp.business.task.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dao.mp.adapter.repository.task.domain.entity.Task;
import com.dao.mp.adapter.repository.task.mapper.TaskMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * mybatis-subject
 *
 * @Auther: cheng.tang
 * @Date: 2022/7/16 8:41 PM
 * @Description:
 */
@Service
@Slf4j
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements TaskService {


}
