package com.dao.mp.adapter.repository.task;

import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dao.mp.MpIntroductionApplication;
import com.dao.mp.adapter.repository.task.domain.entity.Task;
import com.dao.mp.business.task.service.TaskService;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONValue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest(classes = {MpIntroductionApplication.class})
class TaskServiceImplTest {

    @Autowired
    private TaskService taskService;

    @Test
    public void testSave() throws InterruptedException {
        Task task = new Task();
        String flag = String.valueOf(ThreadLocalRandom.current().nextInt());
        task.setName(String.join("_", "任务", flag));
        Timestamp now = new Timestamp(System.currentTimeMillis());
        task.setGmtCreate(now);
        String operator = "operator_" + flag;
        task.setCreateBy(operator);
        task.setModifiedBy(operator);
        boolean saveResult = taskService.save(task);
        log.info("saveResult {} task {} ", saveResult, task);
        task = taskService.getById(task.getId());
        Date firstGmtCreate = task.getGmtCreate();
        log.info("准备更新操作====== firstGmtCreate {} ", firstGmtCreate);
        TimeUnit.SECONDS.sleep(1);
        Task entityUpdate = new Task();
        entityUpdate.setId(task.getId());
        entityUpdate.setName(task.getName() + "更新一下");
        log.info("待更新的数据 entityUpdate {} ", JSON.toJSONString(entityUpdate));
        boolean updateResult = taskService.updateById(entityUpdate);
        log.info(" updateResult {}", updateResult);
        Task taskAfterUpdate = taskService.getById(task.getId());
        log.info("taskAfterUpdate {} ", JSON.toJSONString(taskAfterUpdate));
        assertThat(taskAfterUpdate.getGmtCreate()).isEqualTo(firstGmtCreate);
    }


    @Test
    public void testSelect() {
        List<Task> taskList = taskService.list();
        log.info("taskList {} ", JSONValue.toJSONString(taskList));
    }


    @Test
    public void testWrapperWhen2Gt() {
        QueryWrapper<Task> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id", "name");
        queryWrapper.orderByAsc("id");
        queryWrapper.last("limit 10");
        for (int i = 0; i < 2; i++) {
            queryWrapper.gt("id", 10);
        }
        List<Task> taskList = taskService.list(queryWrapper);
        log.info("{} ", taskList);
    }


    @Test
    public void testSelectLambda() {
        LambdaQueryWrapper<Task> qryUser = Wrappers.lambdaQuery();
        qryUser.in(Task::getId, Collections.singletonList(1L));
        qryUser.select(Task::getId, Task::getName, Task::getDeleted, Task::getGmtModified);
        List<Task> taskList = taskService.list(qryUser);
        log.info("{} ", JSON.toJSONString(taskList));
    }

    @Test
    public void orderByLimitTest() {
        LambdaQueryWrapper<Task> qryRoomUser = Wrappers.lambdaQuery();
        qryRoomUser.eq(Task::getId, 1L);
        qryRoomUser.orderByDesc(Task::getGmtCreate);
        qryRoomUser.last("limit 10");
        List<Task> taskList = taskService.list(qryRoomUser);
        log.info("{} ", JSON.toJSONString(taskList));
    }

    @Test
    public void compareTwoFieldsTest() {
        boolean deeleted = false;
        LambdaQueryWrapper<Task> qryRoom = Wrappers.lambdaQuery();
        qryRoom.eq(deeleted == Boolean.FALSE, Task::getDeleted, deeleted);
        qryRoom.apply("gmt_create!=gmt_modified");
        List<Task> taskList = taskService.list(qryRoom);
        log.info("{} ", JSON.toJSONString(taskList));
    }


}